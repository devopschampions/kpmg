Hello Team, Good Day !! This was implemented in very shot time . Please consier and iam capable to write better than the existing code written which looks more nicer and efficient






Challenge #1
###############

A 3 tier environment is a common setup. Use a tool of your choosing/familiarity create these resources. Please remember we will not be judged on the outcome but more focusing on the approach, style and reproducibility.


Satheesh: I have used the below tools
   
   1) Terrform --> Terraform code to provision 3 tiers ( web tier, app tier and Data tier)
       a) Provision vpc under terraform/vpc ( one done provison step b)
       b) Provision application resources under terraform/dev

          Note: use ur own key for your instance . Update aws_key_pair at terraform/modules/aws-ec2/ec2.tf


   2) Ansible -->  I have used playbook to install application 



Challenge #2
############

Challenge #2

Summary

We need to write code that will query the meta data of an instance within aws and provide a json formatted output. The choice of language and implementation is up to you.

 
Bonus Points

The code allows for a particular data key to be retrieved individually





Satheesh:  Used python script to implement the above code

Steps:
Install Python 3, pipenv
  a) sudo yum install python3
  b)sudo pip3 install pipenv
  c) pipenv install
  d) Go to the folder metadata/src and execute required script
      - python3 metadata.py
      - python3 key.py
Note: you can enable the metadata version using aws cli
https://docs.aws.amazon.com/cli/latest/reference/ec2/modify-instance-metadata-options.html1~



Challenge #3
#################
We have a nested object, we would like a function that you pass in the object and a key and get back the value. How this is implemented is up to you.

 

Example Inputs

object = {“a”:{“b”:{“c”:”d”}}}

key = a/b/c

 

object = {“x”:{“y”:{“z”:”a”}}}

key = x/y/z

value = a


 

 Satheesh:  Used python script to implement the above code
