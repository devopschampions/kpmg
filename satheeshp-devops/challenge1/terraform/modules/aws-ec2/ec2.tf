resource "aws_network_interface" "main" {
  subnet_id       = "${var.subnet_id}"
}
resource "aws_instance" "linux_instance" {
  count 	          = "${var.instance_count}"
  ami                     = "${lookup(var.linux_ami, var.aws_region)}"
  instance_type  	  = "${var.instance_type}"
  disable_api_termination = false
  monitoring              = true
  user_data    	 	  = "${var.user_data}"
  iam_instance_profile    = "${var.iam_instance_profile}"
  key_name                = "${var.key_name}"
  subnet_id               = "${var.subnet_id}"
  

  root_block_device {
    volume_type           = "gp2"
    volume_size           = "${var.volume_size}"
    delete_on_termination = true
  }

  tags			  = {
    Name                  = format("${var.instance_name}-%02d", count.index + 1)
    Type                  = "EC2 Instance"
    Monitoring            = "true"
    Environment           = "${var.environment}"
    Project               = "${var.project}"
	 }

}



resource "aws_key_pair" "kpmg" {
  public_key = "use your aws key"
  }


###################################
#variables
#*********
variable "linux_ami" {
   type = map(string)
   default = {
    ap-southeast-1 = "ami-0615132a0f36d24f4"
        }
}

variable aws_region {
      default = "ap-southeast-1"
}

variable vpc_id {}
variable instance_count {}
variable instance_type {}
variable instance_name {}
variable subnet_id {}
variable sg_id {}
variable subnet_type {}
variable volume_size {}
variable user_data {}
variable iam_instance_profile {
  default = "ec2admin"
}
variable key_name {
  default = "FelixGroup"
}
variable environment {}
variable project {}



##########################################
