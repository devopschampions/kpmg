variable "db_name" {}
variable "storage_size" {}
variable "db_port" {}
variable "db_password" { default = "" }
variable "engine_version" {}
variable "engine_family" { default = "postgres9.6" }
variable "instance_type" {}
variable "multiaz" {  default = true}
variable "vpc_id" {}
variable "backup_window" { default = "03:01-06:00"}
variable "backup_retention_period" { default = "35" }
# backup_retention_period_compliant is the minimum number of days defined in the SCB group policy # Do not change this below!
variable "backup_retention_period_compliant" { default = "35" }
variable "maintenance_window" { default = "Mon:00:00-Mon:03:00"}
variable "auto_minor_version_upgrade" { default = "true" }
variable "skip_final_snapshot" { default = "false" }
variable "final_snapshot_identifier" { default = "final-snapshot-identifier" }
variable "snapshot_identifier" { default = "snapshot-identifier" }


variable "env" {
  type =  map(string)
  default = {
    development = "nonprod"
    testing = "nonprod"
    staging = "prod"
    production = "prod"
  }
}


variable "parameter_group_name" { default = "oracle"}
variable "db_subnet_group_name" {}
variable "vpc_security_group_ids" {}
variable "environment" { default ="dev" }

 variable "alarm_cpu_threshold" {}
 variable "alarm_disk_queue_threshold" {}
 variable "alarm_free_disk_threshold" {}
 variable "alarm_free_memory_threshold" {}
