
resource "aws_db_instance" "mod_rds" {
  instance_class             = "${var.instance_type}"
  storage_type               = "gp2"
  storage_encrypted          = "true"
  allocated_storage          = "${var.storage_size}"
  name                       = "${var.db_name}"
  username                   = "db_admin"
  engine                     = "oracle-se2"
  engine_version              = "12.1.0.2.v26"
  license_model              = "license-included"
  password                   = "dbpassword123"
  port                       = "${var.db_port}"
  publicly_accessible        = false
  multi_az                   = "${var.multiaz}"

  timeouts {
    create = "60m"
    update = "60m"
    delete = "60m"
  }
}


resource "aws_db_parameter_group" "db_parameter_group" {
  name   = "rds-oracle"
  family =  "oracle-se2-12.1"

  parameter {
    name  = ""
    value = "utf8"
  }
}