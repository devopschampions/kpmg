data  "template_file" "inventory" {
    template = "${file("ansible.tpl")}"
    vars = {
        dbendpoint = "${module.devdb.db_endpoint}"
    }
}
resource "local_file" "ansible_inventory" {
  content  = "${data.template_file.inventory.rendered}"
  filename = "../../playbooks/hosts"
}