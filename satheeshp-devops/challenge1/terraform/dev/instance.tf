#### webserver ###

module "webserver" {
    source = "../modules/aws-ec2"
    instance_count          = 2
    instance_type           = "t2.micro"
    instance_name           = "webserver"
    user_data               = file("../../scripts/web.sh")
    volume_size             = 8
    vpc_id                  = "${data.aws_vpc.main.id}"
    environment             = "${var.environment}"
    project                 = "${var.project}"
    subnet_type             = "private"
    subnet_id               = "${data.aws_subnet.private_subnet.id}"
    sg_id                   = "${data.aws_security_groups.app_sg.id}"
}


#### appserver ###

module "appserver" {
    source = "../modules/aws-ec2"
    instance_count          = 2
    instance_type           = "t2.micro"
    instance_name           = "appserver"
    user_data               = file("../../scripts/app.sh")
    volume_size             = 8
    vpc_id                  = "${data.aws_vpc.main.id}"
    environment             = "${var.environment}"
    project                 = "${var.project}"
    subnet_type             = "private"
    subnet_id               = "${data.aws_subnet.private_subnet.id}"
    sg_id                   = "${data.aws_security_groups.app_sg.id}"
}

#### Database ###

module "devdb" {
  source = "../modules/aws-rds-oracle"

  alarm_cpu_threshold         = "75"
  alarm_disk_queue_threshold  = "10"
  alarm_free_disk_threshold   = "5000000000"
  alarm_free_memory_threshold = "128000000"
  db_name                     = "devdb"
  db_port                     = "5401"
  engine_version              = "12.1.0.2.v26"
  instance_type               = "db.m5.large"
  multiaz                     = false
  skip_final_snapshot         = false
  final_snapshot_identifier   = "devdb-backup"
  storage_size                = "1000"
  vpc_id                      = "${data.aws_vpc.main.id}"
  db_subnet_group_name       = "${data.aws_subnets.data_tier.ids}"
  vpc_security_group_ids     = ["${data.aws_security_groups.data_sg.id}"]
}