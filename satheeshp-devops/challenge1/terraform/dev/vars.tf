variable "default_route" {
    default = "0.0.0.0/0"
}


variable "ami_devops" {
  default = "ami-0801a1e12f4a9ccc0"
}

variable "region" {
  default = "ap-southeast-1"
}

variable "instance_type" { default = "" }
variable "profile" { 
  default = "dev" 
}

variable project  { default = "kpmg" }
variable environment { default = "dev" }