data "aws_vpc" "main" {
  filter {
    name = "tag:Name"
    values = ["${var.project}-vpc"]
  }
}

###### Private Network  #######
data "aws_subnets" "app_tier" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.main.id]
  }
}

data "aws_subnet" "private_subnet" {
   filter {
    name = "tag:Name"
    values = ["sub_private_${var.project}"]
  }
}
data "aws_security_groups" "app_sg" {
  filter {
    name = "tag:Name"
    values = ["${var.project}_private_sg"]
        }
}

######## Public Network ##########
data "aws_subnets" "web_tier" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.main.id]
  }
}

data "aws_subnet" "public_subnet" {
   filter {
    name = "tag:Name"
    values = ["sub_public_${var.project}"]
  }
}

data "aws_security_groups" "web_sg" {
  filter {
    name = "tag:Name"
    values = ["${var.project}_public_sg"]
        }
}

############## Data Network ##############

data "aws_subnets" "data_tier" {
   filter {
    name = "tag:Name"
    values = ["sub_data_${var.project}"]
  }
}


data "aws_security_groups" "data_sg" {
  filter {
    name = "tag:Name"
    values = ["${var.project}_data_sg"]
        }
}