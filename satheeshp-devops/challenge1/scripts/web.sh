#!/bin/bash
yum -y update 
yum install -y wget vim tar ksh
sudo amazon-linux-extras install ansible2 -y
curl https://cis-fg.s3.ap-southeast-1.amazonaws.com/playbooks.zip -o playbooks.zip && unzip playbooks.zip
echo "#/bin/bash" > /playbooks/web.sh
echo "ansible-playbook web.yml" >> /playbooks/web.sh
sudo chmod -R 755 /playbooks
sudo cd /playbooks && nohup ./web.sh &